a = {'a': 1, 'b': 4, 't': 67}
b = {'c': 4, 'e': 1, 'a': 4, 't': 7, 'y': 11}

task_2_1 = [key for key in b if key in a]
task_2_2 = [key for key in b if key not in a]
for key, value in b.items():
    map(int, a.keys(), b.keys())
    if key in task_2_1:
        b[key] += a[key]
a.update(b)
print(" Keys in both dictionaries -", task_2_1, '\n',
      "Keys only in the 2nd dictionary -", task_2_2, '\n',
      "Common dictionary -", a)

