import math
prime_num, multiplier, power = [2], [], 0
number = int(input())
for i in range(3, number + 1, 2):
    if all(i % j != 0 for j in range(2, int(math.sqrt(i)) + 1)):
        prime_num.append(i)
for i in prime_num:
    while number > 1 and number % i == 0:
        number /= 2
        power += 1
    print('%s^%d' % (i, power), end='*')
    break
for i in prime_num:
    while number > 1 and number % i == 0:
        number /= i
        multiplier.append(i)
if number > 1:
    multiplier.append(number)
print(*multiplier, sep='*')
