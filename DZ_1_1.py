my_list = [1, 4, 6, 7, 2, 1, 7, 8, 9, 5, 7, 3, 6, 2, 7, 43, 54, 13]
most_common = {}
result = {'max_val -': max(my_list), 'min_val -': min(my_list), 'ind_max_val -': [],
          'ind_min_val -': [], 'most_common_elements -': []}
for i in range(len(my_list)):
    if my_list[i] == max(my_list):
        result['ind_max_val -'].append(i)
    elif my_list[i] == min(my_list):
        result['ind_min_val -'].append(i)
    most_common[my_list[i]] = my_list.count(my_list[i])

for i in range(3):
    for key, value in most_common.items():
        most_common_element = max(set(most_common.values()))
        if value == most_common_element:
            result['most_common_elements -'].append(key)
            most_common[key] = 0
            break
result['task 1.3.1 -'] = list(set(my_list))
result['task 1.3.2 -'] = list(most_common.keys())
[print(key, value) for key, value in result.items()]
